local composer = require( "composer" )
local widget = require( "widget" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen

    local background = display.newRect( sceneGroup, display.contentCenterX, display.contentCenterY, display.actualContentWidth, display.actualContentHeight)
    background:setFillColor( {
        type="gradient",
        color1={ 0.9, 0.2, 0.2}, color2={ 0, 0.8, 0.8}, direction="down"
    } )
 
     local highScore = display.newText(sceneGroup, "HighScore: 0", display.contentCenterX*.4, 50, native.systemFont, 16 )
    highScore:setFillColor( 1, 1, 1)

     local result = display.newText(sceneGroup, "", display.contentCenterX, 350, native.systemFont, 16 )
    highScore:setFillColor( 1, 1, 1)


     local score = display.newText(sceneGroup, "Score: 0", display.contentCenterX, 50, native.systemFont, 16 )
    score:setFillColor( 1, 1, 1 )

    local fakeScore = 0 -- DELETE WHEN BACKEND FINISHED
    local fakeHighScore = 0 -- DELETE WHEN BACKEND FINISHED
    local function handleButtonEvent( event )
        local id = event.target.id -- "higher" or "lower"
        
        local answer = "higher" --CHANGE TO BACKEND ANSWER
        if (math.random(2) - 1 == 0) then
            answer = "lower"
        end
        
        if ( "ended" == event.phase ) then
            print( "Button was pressed and released" )

            if (id == answer) then
            	result.text = "Correct! +1 points!"
                fakeScore = fakeScore + 1
                score.text = "Score: "..fakeScore --CHANGE TO BACKEND USER ANSWER
            else
            	result.text = "incorrect!"
            end

            --CHANGE STUFF TO BACKEND INFO
            if (fakeScore < 15 ) then --CHANGE TO IF GAME NOT FINISHED
                --Go to new card
            else -- game finished
                if (fakeScore > fakeHighScore) then --CHANGE TO IF NEW HIGHSCORE
                    fakeHighScore = fakeScore
                    highScore.text = "HighScore: "..fakeHighScore
                end
                fakeScore = 0
                score.text = "Score: "..fakeScore 
            
            end
        end
    end

    -- Create the widget
    local button1 = widget.newButton(
        {
            left = display.contentCenterX - 110,
            top = display.contentCenterY*1.7,
            id = "higher",
            label = "Higher",
            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            onEvent = handleButtonEvent,
                    -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 100,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={.5,.5,.5,0.5}, over={1,0.1,0.7,0.3} },


        }
    )

    -- Create the widget
    local button1 = widget.newButton(
        {
            left = display.contentCenterX + 10,
            top = display.contentCenterY*1.7,
            id = "lower",
            label = "Lower",
            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            onEvent = handleButtonEvent,
                    -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 100,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={.5,.5,.5,0.5}, over={1,0.1,0.7,0.3} },

        }
    )

end


-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene